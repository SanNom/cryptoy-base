import hashlib
import os
from random import (
    Random,
)

import names


def hash_password(password: str) -> str:
    return hashlib.sha3_256(password.encode()).hexdigest()


def random_salt() -> str:
    return bytes.hex(os.urandom(32))


def generate_users_and_password_hashes(
    passwords: list[str], count: int = 32
) -> dict[str, str]:
    rng = Random()  # noqa: S311

    users_and_password_hashes = {
        names.get_full_name(): hash_password(rng.choice(passwords))
        for _i in range(count)
    }
    return users_and_password_hashes


def attack(passwords: list[str], passwords_database: dict[str, str]) -> dict[str, str]:
    users_and_passwords = {}

    # A implémenter
    # Doit calculer le mots de passe de chaque utilisateur grace à une attaque par dictionnaire
    dict_hash_passw = {}
    i = 0
    for clear_passw in passwords:
        dict_hash_passw[hash_password(clear_passw)] = clear_passw
        i += 1
        if i%100000 == 0:
            print(f"Advancement : {i}")
    print("End hash calc")
    with open("hash.txt", "w") as file:
        file.write(str(dict_hash_passw))

    ret = {}
    for user in passwords_database.keys():
        if passwords_database[user] in dict_hash_passw.keys():
            ret[user] = dict_hash_passw[passwords_database[user]]
        if not user in ret.keys():
            print(f"User : {user} not found in passwords")

    print(ret)
    return ret


def fix(
    passwords: list[str], passwords_database: dict[str, str]
) -> dict[str, dict[str, str]]:
    users_and_passwords = attack(passwords, passwords_database)

    users_and_salt = {}
    new_database = {}

    # A implémenter
    # Doit calculer une nouvelle base de donnée ou chaque élement est un dictionnaire de la forme:
    # {
    #     "password_hash": H,
    #     "password_salt": S,
    # }
    # tel que H = hash_password(S + password)

    for user in users_and_passwords.keys():
        salt = random_salt()
        hash = hash_password(users_and_passwords[user]+salt)
        new_database[user] = {"password_hash": hash, "password_salt": salt}

    return new_database


def authenticate(
    user: str, password: str, new_database: dict[str, dict[str, str]]
) -> bool:
    # Doit renvoyer True si l'utilisateur a envoyé le bon password, False sinon
    print(new_database[user]["password_hash"])
    print(password+new_database[user]["password_salt"])
    return new_database[user]["password_hash"] == hash_password(password+new_database[user]["password_salt"])
