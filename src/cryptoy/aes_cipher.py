from cryptography.hazmat.primitives.ciphers.aead import (
    AESGCM,
)


def encrypt(msg: bytes, key: bytes, nonce: bytes) -> bytes:
    # A implémenter en utilisant la class AESGCM
    aesgcm = AESGCM(key)
    ct = aesgcm.encrypt(nonce, msg, None)
    return ct


def decrypt(msg: bytes, key: bytes, nonce: bytes) -> bytes:
    # A implémenter en utilisant la class AESGCM
    aesgcm = AESGCM(key)
    data = aesgcm.decrypt(nonce, msg, None)

    return data
