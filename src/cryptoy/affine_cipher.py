from math import (
    gcd,
)

from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)

# TP: Chiffrement affine


def compute_permutation(a: int, b: int, n: int) -> list[int]:
    # A implémenter, en sortie on doit avoir une liste result tel que result[i] == (a * i + b) % n
    ret = []
    for i in range(n):
        ret.append((a*i+b)%n)
    return ret


def compute_inverse_permutation(a: int, b: int, n: int) -> list[int]:
    # A implémenter, pour cela on appelle perm = compute_permutation(a, b, n) et on calcule la permutation inverse
    # result qui est telle que: perm[i] == j implique result[j] == i
    perm = compute_permutation(a, b, n)
    result = [0]*len(perm)
    for i in range(len(perm)):
        j = perm[i]
        result[j] = i
    return result



def encrypt(msg: str, a: int, b: int) -> str:
    # A implémenter, en utilisant compute_permutation, str_to_unicodes et unicodes_to_str
    '''
    - Encoder tout l'alphabet
    - Encoder chaque caractere avec sa nouvelle position dans l'alphabet
    '''
    int_msg = str_to_unicodes(msg)
    cyph = compute_permutation(a, b, 0x110000)
    ret = ""
    for i in range(len(int_msg)):
        ret += chr(cyph[int_msg[i]])

    print(f"{msg} -> {ret}")
    return ret

def encrypt_optimized(msg: str, a: int, b: int) -> str:
    # A implémenter, sans utiliser compute_permutation
    int_msg = str_to_unicodes(msg)
    ret = ""
    for i in range(len(int_msg)):
        new_c = (a*int_msg[i]+b) % 0x110000
        ret += chr(new_c)

    #print(f"{msg} -> {ret}")
    return ret


def decrypt(msg: str, a: int, b: int) -> str:
    # A implémenter, en utilisant compute_inverse_permutation, str_to_unicodes et unicodes_to_str
    '''
    a = 1804
    b = 4246
    to_crypt = 600
    n = 0x110000
    crypt = (a*to_crypt+b) % n
    decrypt = (crypt - b) / a

    a*to_crypt+b ne doit pas être supérieur à n, sinon pas dechiffrable
    '''
    n = 0x110000
    cyph_alph = compute_inverse_permutation(a, b, n)
    int_msg = str_to_unicodes(msg)
    ret = ""
    for i in range(len(msg)):
        ret += chr(cyph_alph[int_msg[i]])
    return ret

def decrypt_optimized(msg: str, a_inverse: int, b: int) -> str:
    # A implémenter, sans utiliser compute_inverse_permutation
    # On suppose que a_inverse a été précalculé en utilisant compute_affine_key_inverse, et passé
    # a la fonction
    ret = ""
    int_msg = str_to_unicodes(msg)
    for i in range(len(msg)):
        old_c = int((a_inverse*(int_msg[i] - b)) % 0x110000)
        ret += chr(old_c)
    return ret

def compute_affine_keys(n: int) -> list[int]:
    # A implémenter, doit calculer l'ensemble des nombre a entre 1 et n tel que gcd(a, n) == 1
    # c'est à dire les nombres premiers avec n
    '''def gcd(a, n):
        gcd = 1
        for i in range(n, 1, -1):
            if n%i == a%i == 0:
                return gcd'''


    ret = []
    for a in range(2,n):
        if gcd(a, n) == 1:
            ret.append(a)

    return ret


def compute_affine_key_inverse(a: int, affine_keys: list, n: int) -> int:
    # Trouver a_1 dans affine_keys tel que a * a_1 % N == 1 et le renvoyer
    # Placer le code ici (une boucle)

    #print("a : " + str(a))
    #print("n : " + str(n))
    for a_1 in affine_keys:

        if (a*a_1)%n == 1:
            #print("Found inverse : " + str(a_1))
            return a_1

    # Si a_1 n'existe pas, alors a n'a pas d'inverse, on lance une erreur:
    raise RuntimeError(f"{a} has no inverse")


def attack() -> tuple[str, tuple[int, int]]:
    s = "࠾ੵΚઐ௯ஹઐૡΚૡೢఊஞ௯\u0c5bૡీੵΚ៚Κஞїᣍફ௯ஞૡΚր\u05ecՊՊΚஞૡΚՊեԯՊ؇ԯրՊրր"
    # trouver msg, a et b tel que affine_cipher_encrypt(msg, a, b) == s
    # avec comme info: "bombe" in msg et b == 58

    # Placer le code ici

    n = 0x110000

    affine_keys = compute_affine_keys(n)
    print("Nbr affine keys : " + str(len(affine_keys)))
    #compute_affine_key_inverse(a, affine_keys, n)
    b = 58
    while b < n:
        print("B : " + str(b))
        for i in range(len(affine_keys)):
            if i%10000 == 0:
                print("i : " + str(i))
            a = affine_keys[i]
            bombe_enc = encrypt_optimized("bombe", a, b)

            if bombe_enc in s:
                print(f"FOUND A : {a} | B : {b}")
                clear_s = decrypt(s, a, b)
                print("Clear s : " + str(clear_s))
                return (clear_s, (a, b))
                # FOUND A : 27 | B : 58
        b += 1


    raise RuntimeError("Failed to attack")


def attack_optimized() -> tuple[str, tuple[int, int]]:
    s = (
        "જഏ൮ൈ\u0c51ܲ೩\u0c51൛൛అ౷\u0c51ܲഢൈᘝఫᘝా\u0c51\u0cfc൮ܲఅܲᘝ൮ᘝܲాᘝఫಊಝ"
        "\u0c64\u0c64ൈᘝࠖܲೖఅܲఘഏ೩ఘ\u0c51ܲ\u0c51൛൮ܲఅ\u0cfc\u0cfcඁೖᘝ\u0c51"
    )
    # trouver msg, a et b tel que affine_cipher_encrypt(msg, a, b) == s
    # avec comme info: "bombe" in msg

    # Placer le code ici

    n = 0x110000

    affine_keys = compute_affine_keys(n)
    print("Nbr affine keys : " + str(len(affine_keys)))
    b = 1
    while b < n:
        print("B : " + str(b))
        for a in affine_keys:
            print(a)
            a_inverse = compute_affine_key_inverse(a, affine_keys, n)
            bombe_dec = decrypt_optimized(s, a_inverse, b)

            if "bombe" in bombe_dec:
                print(f"FOUND A : {a} | B : {b}")
                print("Clear s : " + str(bombe_dec))
                return (bombe_dec, (a, b))
        b += 1

    raise RuntimeError("Failed to attack")
